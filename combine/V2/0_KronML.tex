





\documentclass[journal]{IEEEtran}
\usepackage{amsmath,amssymb}












\usepackage{cite}






\ifCLASSINFOpdf
  \usepackage[pdftex]{graphicx}
  \graphicspath{{../pdf/}{../jpeg/}}
  \else
  \usepackage[dvips]{graphicx}
  \DeclareGraphicsExtensions{.eps}
\fi





\usepackage{amsmath}
\interdisplaylinepenalty=2500









\usepackage{array}











\usepackage{stfloats}

\usepackage{xcolor}
\usepackage{siunitx}






\usepackage{url}





\DeclareMathOperator{\vect}{vec}

\hyphenation{op-tical net-works semi-conduc-tor}

\usepackage{amsfonts,amssymb}
\usepackage{bm}
\usepackage{tabularx}
\usepackage{siunitx}

\makeatletter
\let\MYcaption\@makecaption
\makeatother

\usepackage[font=footnotesize]{subcaption}

\makeatletter
\let\@makecaption\MYcaption
\makeatother

\begin{document}
\title{ Tensorization of Multi-Frequency PolSAR Data for Classification Using an Auto-Encoder Network}

\author{Shaunak De,~\IEEEmembership{Student Member,~IEEE,}
        Debanshu Ratha,~\IEEEmembership{Student Member,~IEEE,}
        Dikshya Ratha,~\IEEEmembership{} \\
        Avik Bhattacharya,~\IEEEmembership{Senior Member,~IEEE,}
        and Subhasis Chaudhuri,~\IEEEmembership{Fellow,~IEEE}
} 















\maketitle



\begin{abstract}
A novel tensorization framework is proposed which utilizes the Kronecker product to combine multi-frequency PolSAR data in conjunction with an Artificial Neural Network (ANN) for classification. 
The ANN comprises of two stages, where an unsupervised stochastic sampling Auto-Encoder (AE) learns an efficient representation, and a supervised Feed Forward (FF) network performs classification.
The proposed framework is demonstrated using multi-frequency (C-, L-, P- band) datasets collected by the AIRSAR system. 
The classification performance of single, tensor products of dual, and triple band combinations are evaluated. It is observed that the classification accuracy of the tensor products outperform single, as well as, the simple augmentation of the frequency bands.




	
	
	
	
	
	
	

\end{abstract}

\begin{IEEEkeywords}
Tensor Product, Neural Network, Polarimetric SAR, Radar, Multi-frequency, Classification
\end{IEEEkeywords}





\IEEEpeerreviewmaketitle





\section{Introduction}
\IEEEPARstart{F}{ull} Polarimetric SAR (PolSAR) is an advanced imaging technique which coherently transmits and receives radar pulses in quadrature polarization. The multi-frequency polarized waves are susceptible to different structure, orientation and dielectric properties of the medium. Various PolSAR systems (viz. AIRSAR, EMISAR, and F-SAR) are capable of simultaneous multi-frequency full PolSAR data acquisition allowing for improved target characterization~\cite{499786}.  



The use of single-frequency and single-polarization SAR data has limited success in land cover classification, even when temporal changes in the scattering are exploited, especially for agricultural applications~\cite{mcnairn2009contribution}.
Techniques like hierarchical segmentation \cite{liu2016hierarchical}  and sketch maps in conjunction with adaptive Markov random fields~\cite{Junfei2016} have demonstrated to obtain an accurate classification for single frequency PolSAR data by leveraging contextual information while preserving edges.
Early studies~\cite{baronti1995sar,ferrazzoli1997potential} have evaluated the effectiveness of multi-frequency PolSAR data acquisition for crop identification. Furthermore, an exhaustive comparison on the use of multi-frequency polarimetric acquisition was performed in~\cite{lee2001quantitative}, with the assessment of quantitative classification accuracy. It was found that the integration of polarimetric information from multiple bands leads to an improved classification performance.  Different strategies have been adopted to combine information from multi-frequency observations. An early approach for multi-frequency PolSAR classification is described in~\cite{499786}. It uses a dynamic neural network to combine the information from C-, L- and P-bands for classification.
An unsupervised classification technique using dual-frequency PolSAR datasets is introduced in~\cite{964969Famil2001}. 
A $6\times6$ polarimetric coherency matrix is constructed to combine the multi-frequency information. The methodology uses an iterative algorithm based on a complex Wishart density function to classify the multi-frequency data.
In~\cite{hoekman2003new}, a matrix reversible transformation approach is developed to combine multi-frequency PolSAR datasets into a representation with a simplified statistical description for classification. However, the approach requires extensive \emph{a-priori} knowledge of the datasets leading to a redundant representation.





The combination of multi-frequency information causes a simultaneous increase in the dimensionality of the feature set. A classification task is often easier to solve in a higher dimensional space. 
In this paper, a novel tensorization framework utilizing the Kronecker product for the combination of multi-frequency PolSAR data, and its subsequent classification using an Artificial Neural Network (ANN) is proposed. The tensorization leads to increased dimensionality, which is exploited by an ANN architecture to achieve improved classification performance over simple augmentation of data from multi-frequency bands. 
The ANN comprises of two stages where an unsupervised stochastic sampling Auto-Encoder (AE) learns an efficient representation and a supervised Feed Forward (FF) network performs classification.
The proposed framework is demonstrated for multi-frequency classification of an agricultural and a forested scene.








\section{Methodology}
The methodology is divided into two components: Synthesis of the feature vector using tensorization (Section~\ref{sec:tensor}) and classification using an ANN (Section~\ref{sec:ANN}). 
\subsection{Tensorization}
\label{sec:tensor}
A tensorization framework is proposed which utilizes the Kronecker product of two matrices $\mathbf{A} \in \mathbb{F}^{p\times q}$ and $\mathbf{B} \in \mathbb{F}^{r \times s}$ denoted as $\mathbf{A} \otimes \mathbf{B}$ and is defined as, 
\begin{equation*} 
\mathbf{A} \otimes \mathbf{B} = \left[\begin{array}{rrrr}
a_{11} \mathbf{B}  & a_{12}\mathbf{B}  & \dots  & a_{1q} \mathbf{B}\\
\vdots  & \vdots   & \ddots   & \vdots\\
a_{p1} \mathbf{B} & a_{p2}\mathbf{B} & \dots & a_{pq} \mathbf{B}      
\end{array}\right]_{pr \times qs}
\end{equation*}  
where $\mathbb{F}$ is a field such as $\mathbb{R}$ or $\mathbb{C}$. The matrix block  $a_{ij}\mathbf{B}$ is of dimension of $\mathbf{B}$.
In general, the Kronecker product of two matrices is non-commutative, meaning that $\mathbf{A}\otimes \mathbf{B} \ne \mathbf{B}\otimes \mathbf{A}$. Each entry of $\mathbf{A} \otimes \mathbf{B}$ (or $\mathbf{B}\otimes \mathbf{A}$) is of the form $a_{ij}b_{kl}$ (or $b_{kl}a_{ij}$). Thus $\mathbf{B}\otimes \mathbf{A}$ can be obtained by permuting the entries of $\mathbf{A}\otimes \mathbf{B}$.

In PolSAR literature, the Kronecker product is used to derive the Kennaugh matrix from the scattering matrix $  \mathbf{S} $.  
A radar target is characterized by a scattering matrix $\mathbf{S}$ which describes the dependence of scattering properties on polarization. It is defined in the $hv$ basis as,
\begin{equation}
\bm{\mathbf{S}} =
  \begin{bmatrix}
    S_{hh} & S_{hv}  \\
    S_{vh} & S_{vv}
  \end{bmatrix}
 \label{eqn:scattring}
\end{equation}
where each element is a complex quantity composed of the amplitude and the phase of the scattered electromagnetic signal.
The Pauli target vector is generated from $\mathbf{S}$ as,
\begin{equation}
\bm{k_p} = \frac{1}{\sqrt{2}} \left[ S_{hh}+S_{vv} \quad S_{hh}-S_{vv} \quad  2S_{hv} \right] ^{T}
\end{equation}
which is then utilized to obtain the coherency matrix, $\mathbf{T}$, by spatial ensemble averaging,
\begin{equation}
\mathbf{T} = \frac{1}{N} \sum_{i=1}^{N} \bm{k_{p_i}}\bm{k_{p_i}}^{\dagger} 
\end{equation}
where $\dagger$ is the conjugate transpose. The coherency matrix $\mathbf{T}$ is a hermitian positive semi-definite matrix, and thus  can be diagonalized as,
\begin{equation}
\mathbf{T}  = \mathbf{U}  \mathbf{ \Lambda }\mathbf{ U^{\dagger}} = \sum\limits_{i=1}^{3}\lambda_{i}\bm{u_i} \bm{u_i}^{\dagger}
\end{equation}
where $\mathbf{ \Lambda}$ is a $3\times3$ diagonal matrix with non-negative real eigenvalues, and $\mathbf{U} = [\bm{u_1}\ \bm{u_2}\ \bm{u_3}]$ is a unitary matrix of the SU(3) group, where $\bm{u_1}$, $\bm{u_2}$, and $\bm{u_3}$ are the orthogonal eigenvectors corresponding to the eigen values $\lambda_1,\lambda_2,\lambda_3$. 

If $ \mathbf{T}_{1} = \mathbf{U_1}\mathbf{\Lambda_1}\mathbf{U_1^\dagger}$ and $ \ \mathbf{T}_{2} = \mathbf{U_2}\mathbf{\Lambda_2}\mathbf{U_2^\dagger}$ are the eigenvalue/eigenvector decompositions of $\mathbf{T}_{1}$ and $\mathbf{T}_{2}$ respectively, then,
\begin{equation}
\mathbf{T}_{1}\otimes\mathbf{T}_{2}=(\mathbf{U_1}\otimes \mathbf{U_2})(\mathbf{\Lambda_1}\otimes\mathbf{\Lambda_2})(\mathbf{U_1}\otimes \mathbf{U_2})^{\dagger}
\end{equation}
is the eigen decomposition of $\mathbf{T}_{1}\otimes\mathbf{T}_{2}$ utilizing the mixed Kronecker product properties. Here $\mathbf{T_k}$ corresponds to the frequency band $f_k$.  
Hence, using the Kronecker product the diagonal matrices corresponding to two or more frequencies may be combined to obtain the tensorized product, 
\begin{equation}
\mathbf{X}_{f_1, f_2, \ldots ,f_k}  =  \Lambda_{f_1} \otimes \Lambda_{f_2} \otimes \ldots \otimes {\Lambda_{f_k}}.
\end{equation}
The non-zero elements of the matrix $\mathbf{X}_{f_1, f_2, \ldots ,f_k}$ is used as input vector $\bm{x}$ in the  ANN. The eigen decomposition of the Kronecker product of the coherency matrices corresponding to C-, L- and P-band is used to generate feature vectors for classification. 

Current technology allows for simultaneous multi-frequency acquisition of PolSAR data of the same scene. Thus, efficient techniques for augmentation and fusion of the acquired data for the analysis of enhanced information content is desirable. The proposed tensorization framework allows for homogeneous composition of information from each frequency-band and prevents an \emph{a-priori} bias towards a dominant band. Hence, such a combination leads to a higher dimensional representation which, in general, is more separable for classification tasks if exploited by suitable learning algorithms. 





\begin{figure*} 
	\centering
	\includegraphics[width=0.77\textwidth]{012_Figures+AE}
	\caption{Schematic diagram of the proposed framework.}
	\label{fig:ANN}
\end{figure*}

\subsection{Artificial Neural Network}
\label{sec:ANN}
The ANN architecture used in this work is divided into two stages as shown in Figure~\ref{fig:ANN}. 
First, an unsupervised stochastic sampling sparse stacked Auto-Encoder (AE) stage learns an efficient intermediate representation of the combined multi-frequency features. Subsequently, this learned representation is classified by a FF network. 

The AE is a  fully connected, feed-forward, non-recurrent neural network with multiple hidden layers. 
The number of nodes in the input and output layers is equal. It consists of three parts: the encoder, the decoder and a representational layer ($\bm{z}$) as shown in Figure~\ref{fig:ANN}. During the training phase, the output nodes $\bm{x_i'}$ are connected to the input nodes $\bm{x_i}$. For each pixel $i$, the AE attempts to reconstruct the  $\bm{x_i}$ as  $\bm{x_i'}$. The reconstruction error is computed and minimized over multiple iterations in order to learn an optimal reconstruction. 

The AE is preceded by a  stochastic sampling block which randomly selects a vector as $\bm{x_i}$ from a $N\times N$ window in each iteration as input vector and target while training the network. This is akin to using a smoothing filter and leads to  improved generalization performance of the AE in the presence of speckle~\cite{zeiler2013stochastic}.
Additionally, the windows are also chosen in random order to prevent the AE from memorizing the order of input. 



This representation is built in an unsupervised manner for each pixel in the dataset. The fundamental computational unit of an AE is a connected neuron that takes as input $\bm{x_i} = \left[ x_1, x_2, \ldots ,x_n \right]$ and a bias intercept term $b$ and produces the hypothesis $h = \mathbf{s}(\sum_{i=0}^{n} \mathbf{W_{r_i}} \bm{x_i} + b)$, where $\mathbf{s}$ is a non-saturating non-linearity called  Parametric Rectified Linear Unit (PReLU). 
The response of $\mathbf{s}(y_i)=y_i$ when $y_i>0$ and $\mathbf{s}(y_i)=a_iy_i$ when $y_i<0$. Here, $a_i$ is a learnable parameter controlling the slope of the negative response of the PReLU~\cite{he2015delving}. 
As the width of the input vector increases with the tensorial combination of multiple frequencies, the ReLUs tend to have difficulties with zero gradients during back-propagation. This prevents  efficient training of the AE. Hence, the use of PReLUs helps to overcome this problem and improve the convergence rate of the network.   

The representation $\bm{x}$ has relatively high dimensionality with redundancies.
This makes classification difficult, which either leads to poor performance or requires a more complex classification scheme. However, an optimal representation learned by the AE simplifies the classification. It is observed that even with a relatively simple FF network, a high level of accuracy is obtained.  

On completion of the training phase, the update of weights in the AE are suspended, and the decoder is disconnected. 
The representation $\bm{z_i}$ is extracted for each pixel $i$ and is used as input in the FF network stage. 
The final classification is performed in a supervised manner by the FF network. During this stage, only the labeled training pixels are considered. $\bm{z_i}$ is applied as an input to the FF network and the labels $L_i$ are applied at the output. The error between the predicted and actual labels is minimized over multiple iterations with only the weights of the FF network being updated while utilizing the representation learned in the training phase from the AE. The terminal layer of the FF uses Softmax saturating non-linearities scaled in the range $[0,1]$, instead of PReLU. Here a value close to 1 indicates an active response to the corresponding label, while 0 represents an inactive one.  


The proposed architecture allows computation of classification confidence for each pixel as a part of the FF network. The number of nodes in the output layer is chosen to be one more than the minimum required to represent every class. This node, called zero (or null) label, remains unmapped to any label and allows the estimation of uncertainty of classification.
The null label represents those data points which have not been successfully classified into one of the given set of labels.
This is used to estimate the overall confidence level of the classified output by the network and simultaneously exclude pixels with high uncertainty from the final output. 
In practice, the total labeled pixels are randomly divided into three groups, training, test and validation. The fine tuning is conducted only on the training pixels, with the test pixels used to check for over-fitting at regular intervals. The network performance is measured at the end of classification over the validation pixels. 

 





%subsequent inputs.
\section{Experiment}

The ANN consists of two stages, an AE and a FF network. The AE consists of 5 hidden layers: 2 encode, 2 decode and 1 representational layer. The FF consists of 3 feed forward fully connected layers. First, the AE is trained in an unsupervised manner under the constraint of minimization of cross-entropy error between input $\bm{x_i}$ and output $\bm{x_i'}$ with the FF disconnected (see Figure~\ref{fig:ANN}). The learning rate is $l_r = 10^{-4}$ with a reduction by a factor of $\Gamma=0.1$ at every 2 training epochs. Optimization is performed using the ADAM solver with a momentum $\mu=0.95$ and weight decay $\beta=0.005$. The total labeled data is divided into 3 parts: training, test and validation which are selected at random. The proportions are 10\%, 30\% and 60\% of the total labeled data respectively. The training is balanced by selecting a fixed number of pixels per class randomly from the 10\% training pool.
The accuracy statistics are reported as an average of 5 runs on the validation set.  

After the training stage of the AE is complete, the representational layer is extracted and given as input to the FF network. The FF network is trained in a supervised manner with $l_r = 10^{-5}$, $\mu=0.92$ and $\beta=0.005$. The classification is obtained as output from the terminal layer of the FF network. 

For comparison, the individual bands ($C$, $L$ and $P$), tensorized band pairs ($CL$, $CP$, $LP$),  tensorized triplet ($CLP$) and a simple band augmentation ($CLP_{+}$) are considered. For the individual bands, the input vector $\bm{x}$ is of dimension $1\times3$, while for tensorized pairs and triplets,  $\bm{x}$ is of dimension $1\times9$ and $1\times27$ respectively. 
The benefit of using the tensorization framework is demonstrated by comparing it  against the simple augmentation of all frequency bands. In the augmented case, the $1\times9$ resultant input vector is formed by appending individual frequency input vectors. The proposed framework is also compared with standard classification techniques like Support Vector Machine with a Radial Basis Function kernel (SVM-RBF) and Extreme Learning Machine
(ELM) network. The parameters used for comparison are automatically determined by standard techniques. 
For the SVM, $\gamma=0.125$ and $C=2^{-4}$. In the ELM, $N=1200$ with a sigmoid activation function is chosen.

It may be noted, that even though the Kronecker product operation is non-commutative, the operand ordering has no significant effect on the proposed algorithm since the AE is insensitive to the sequence of elements in the input vector. 
The null label node in the output layer is used to estimate the classification performance while qualitatively improving the results. If the output activation level of the node corresponding to the null label exceeds that of the other nodes, the corresponding pixel is considered unclassified and is excluded from the final classification output. The average normalized activation  of the successfully classified pixels is used as an estimation of the classification confidence. 






% if Comparison is done with simple augmentation
\section{Dataset and Study Area}

Two multi-frequency polarimetric SAR datasets from the AIRSAR system are used in this study. A C-, L-, P-band dataset  acquired over and agricultural area in Flevoland, Netherlands on 3 July 1991, and a C-, L-band dataset acquired over a forested area in Landes, France on 20 June 1991.
These 16-look datasets have a slant range resolution of \SI{6.66}{m} and an azimuth resolution of \SI{8.20}{m}. 
The Flevoland site consists of an agricultural area with large uniform fields with an average area of \SI{\sim20}{ha}. Extensive ground truth information from the campaign is available in literature~\cite{vissers1992groundtruth}.  
A $750\times700$ pixel subset is used to present the results in this paper. The crops present in this subset are Peas, Wheat, Rapeseed (R.Seed), Lucerne, Barley, Potato and Beet. Most of the crops are in the middle of their growth stage.  
The Landes site is a pine forest, with tree-patches of differing ages. A reference map is constructed from observation of polarimetric signatures.





\section{Results and Discussion}
\begin{figure}[t]
\centering
    \begin{subfigure}[b]{0.115\textwidth}
        \includegraphics[width=\textwidth]{013_Figures+Validation_COLOUR}
        \caption{}
        \label{fig:Training}
    \end{subfigure}
     \begin{subfigure}[b]{0.115\textwidth}
        \includegraphics[width=\textwidth]{014_Figures+C_COLOUR}
        \caption{}
        \label{fig:C}
    \end{subfigure}
    \begin{subfigure}[b]{0.115\textwidth}
        \includegraphics[width=\textwidth]{015_Figures+Ls_COLOUR}
        \caption{}
        \label{fig:L}
    \end{subfigure}
    \begin{subfigure}[b]{0.115\textwidth}
        \includegraphics[width=\textwidth]{016_Figures+P_COLOUR}
        \caption{}
        \label{fig:P}
    \end{subfigure}
\begin{tabular}{llllllll}
\includegraphics[width=0.01\textwidth]{017_Figures+Legend+Pea} & Peas & \includegraphics[width=0.01\textwidth]{018_Figures+Legend+Wheat} & Wheat & \includegraphics[width=0.01\textwidth]{019_Figures+Legend+Rseed} & R.Seed & \includegraphics[width=0.01\textwidth]{020_Figures+Legend+Luc} & Lucerne  \\
\includegraphics[width=0.01\textwidth]{021_Figures+Legend+Barley} & Barley & \includegraphics[width=0.01\textwidth]{022_Figures+Legend+Potatoe} & Potato  & \includegraphics[width=0.01\textwidth]{023_Figures+Legend+Beet} & Beet &  & 
\end{tabular}
\caption{(a) Ground Truth.  Classification outputs of input vectors derived from individual (b) $C$ (c) $L$ (d) $P$ bands. }\label{fig:SingleBand}
\end{figure}



\begin{figure*}[t]
\centering
        \begin{subfigure}[b]{0.12\textwidth}
            \includegraphics[width=\textwidth]{024_Figures+CL_COLOUR}
            \caption{}
            \label{fig:CL}
        \end{subfigure}
        ~ \begin{subfigure}[b]{0.12\textwidth}
            \includegraphics[width=\textwidth]{025_Figures+CP_COLOUR}
            \caption{}
            \label{fig:CP}
        \end{subfigure}
        ~ \begin{subfigure}[b]{0.12\textwidth}
            \includegraphics[width=\textwidth]{026_Figures+LP_COLOUR}
            \caption{}
            \label{fig:LP}
        \end{subfigure}
            ~ \begin{subfigure}[b]{0.12\textwidth}
                \includegraphics[width=\textwidth]{027_Figures+CLP_COLOUR}
                \caption{}
                \label{fig:CLP}
            \end{subfigure}
        ~
        \begin{subfigure}[b]{0.12\textwidth}
            \includegraphics[width=\textwidth]{028_Figures+CLP2_COLOUR}
            \caption{}
            \label{fig:CLPaug}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.12\textwidth}
                  \includegraphics[width=\textwidth]{029_Figures+SVM_COLOUR}
                  \caption{}
                  \label{fig:SVM}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.12\textwidth}
                        \includegraphics[width=\textwidth]{030_Figures+ELM_COLOR}
                        \caption{}
                        \label{fig:ELM}
        \end{subfigure}
\caption{Classification outputs of input vectors derived from (a) $CL$ (b) $CP$ (c) $LP$ (d) $CPL$ (e) $CLP_{+}$ band combinations with the proposed method and using (f) SVM-RBF (g) ELM methods. The SVM-RBF and ELM methods do not inherently generate a confidence map and are presented unmasked.}\label{fig:MultiFreq}
\end{figure*}






\subsection{Flevoland Dataset}
The result of classification using individual frequency bands is shown in Figure~\ref{fig:SingleBand}. The L-band has the best classification accuracy amongst the three individual bands with an overall accuracy of $90.86\%$ and Cohen's kappa ($\kappa$) of $0.64$. This is followed by the P-band with $90.75\%$ and C-band with $89.79\%$. 
Classwise accuracies for each band or band-combinations are shown in Table~\ref{tab:class}. The values tabulated in bold indicate the best performance combination for the particular class. 
The longer wavelength of the P-band allows it a high penetration capability. This renders broad leaf, sparse crops like peas, potatoes and sugar beet nearly invisible to the radar leading to misclassification. Even at a comparatively shorter wavelength of L-band, the accuracy of classification of the pea crop is only $26.21\%$. 
At this frequency band, it is mostly confused with wheat. 
The best classification for this crop is obtained using C-band with a classification accuracy of $40.01\%$. 

Sugar-beet which has a height of \SI{24}-\SI{30}{cm} and has a cover of about $40\% - 60\%$ is also misclassified by all three bands. 
Potato crops are taller and have a height of \SI{50}-\SI{60}{cm} with a cover of $90\% - 95\%$. These are classified reasonably well with the C-band with an accuracy of 71.16\%, but poorly so with L- and P-band with accuracies of $30.80\%$ and $30.93\%$, respectively. 
Long stem vegetative crops like wheat, with a cover of $85\% - 95\%$ and a height of \SI{85}-\SI{95}{cm}, are well classified with all three frequencies. 
This disparity in performance is indicative of the differences in information content for each crop at different sensing wavelengths. This can be potentially exploited by combining information from different bands to improve the classification performance. 






The result of multi-frequency classifications with the proposed approach is shown in Figure~\ref{fig:MultiFreq}. The combination of information from different bands is able to improve the classification accuracy. The overall accuracies for individual and combined frequency bands along with two standard methods are reported in Table~\ref{tab:OAnACC}. 
It can be seen that the tensor combination of all three bands is the most informative and has the best classification performance with an overall accuracy of $98.23\%$ with $\kappa = 0.94$. In this combination, all classes are correctly identified with the potatoes and sugar beet having the least accuracies at $91.97\%$ and $89.82\%$, respectively. These classes, however, have not been significantly confused with the others, rather they have remained unclassified due to low confidence. This may be due to broad leaves and the relative size of the crop.
The performance of the proposed algorithm is identical to the state-of-the art approach reported in~\cite{7375754}. However, the proposed method being non-parametric makes no \textit{a-priori} assumptions about the distribution of the data. 


Among the dual frequency combinations,  $CP$ has the highest performance with an overall classification accuracy of $96.59\%$ with $\kappa = 0.88$. This combination contains the most diverse information, with the $C$ band being sensitive to scattering from the vegetation canopy, while the $P$ band is able to penetrate into deeper layers of vegetation. This is followed by the $LP$ combination with an overall classification accuracy of $96.40\%$ with $\kappa = 0.87$. Finally, the $CL$ combination has the lowest performance, with an overall accuracy of $95.57\%$ with $\kappa = 0.84$. However, it outperforms classification using individual bands. 
The multilayer information content carried by each (C-, L-, P-) band allows for better crop classification by integrating multi-frequency information. 
It is also seen that tensorization ($CLP$) is able to outperform simple band augmentation ($CLP_{+}$), and also the SVM-RBF and ELM approaches. Since the confidence map generation was not a part of the SVM-RBF and ELM techniques, they are presented unmasked. However explicit generation of confidence maps might be possible with different strategies in each technique.
The confusion matrix for classification using $CLP$ combination by the proposed method is presented in Table~\ref{tab:cmCLP}. Wheat is the most significant class in the dataset and is classified with an accuracy of $93.91\%$, with the rest being confused with barley. Among the crops present, the beet crop is misclassified with most other crops.


\subsection{Landes Dataset}
To establish the robustness of the results an experiment is conducted on the Landes dataset with two frequency bands. The classification results are presented in Figure~\ref{fig:secondSet}. 
The tensorized pair $CL$ has an overall accuracy of $96.23\%$ with $\kappa=0.94$. This exceeds the classification performance obtained by simple augmentation ($CL_+$), which has an overall accuracy of $92.15\%$ with $\kappa=0.91$. For individual bands, the L-band, with an overall accuracy of  $89.73\%$ with $\kappa=0.70$, outperforms the C-band alone, which has an overall accuracy of  $77.42\%$ with $\kappa=0.62$. In this case, the greater penetration ability of L-band is crucial in being able to segregate the age of the trees, since return from the top foliage of the tree is nearly identical irrespective of their ages. 

\begin{figure}[t]
	\centering
	\begin{subfigure}[b]{0.115\textwidth}
		\includegraphics[width=\textwidth]{031_Figures+Review+GT}
		\caption{}
		\label{fig:Training}
	\end{subfigure}
	\begin{subfigure}[b]{0.115\textwidth}
		\includegraphics[width=\textwidth]{032_Figures+Review+CL_raw_COLOUR_PR_0_185_1000_1050_MASK}
		\caption{}
		\label{fig:C}
	\end{subfigure}
	\begin{subfigure}[b]{0.115\textwidth}
		\includegraphics[width=\textwidth]{033_Figures+Review+C+L_RAW+COLOUR_CROP_MASK}
		\caption{}
		\label{fig:L}
	\end{subfigure}
	\begin{subfigure}[b]{0.115\textwidth}
		\includegraphics[width=\textwidth]{034_Figures+Review+L_COLOUR_PR_CROP_MASK}
		\caption{}
		\label{fig:P}
	\end{subfigure}
	\begin{tabular}{lllllllllll}
		\includegraphics[width=0.01\textwidth]{035_Figures+Legend+Pea} & C1 & \includegraphics[width=0.01\textwidth]{036_Figures+Legend+Wheat} & C2 & \includegraphics[width=0.01\textwidth]{037_Figures+Legend+Rseed} & C3 & \includegraphics[width=0.01\textwidth]{038_Figures+Legend+Luc} & C4 &
		\includegraphics[width=0.01\textwidth]{039_Figures+Legend+Barley} & C5 &
		 \end{tabular}
	\caption{(a) Reference Image.  Classification outputs of input vectors derived from individual (b) $CL$ (c) $CL_+$ (d) $L$ bands. }\label{fig:secondSet}
\end{figure}









\begin{table}[!b]
    \centering
    \caption{Class Wise Overall Accuracy}
    \label{tab:class}
    \begin{tabularx}{\columnwidth}{XXXXXXXXX}
        \hline \noalign{\vskip 0.5mm} 
        $\diagup$ & $C$    & $L$    & $P$    & $CL$   & $LP$   & $CP$   & $CLP$ & $CLP_+$  \\ \hline \noalign{\vskip 0.3mm}
        Peas     & 0.40 & 0.26 & 0.00 & 0.84 & 0.66 & 0.99 & \textbf{1.00} & 0.86\\
        Wheat    & 0.70 & 0.81 & 0.91 & 0.84 & 0.89 & \textbf{0.95} & 0.93 & 0.90\\
        R.Seed & 0.81 & 0.97 & 0.88 & 0.99 & 0.99 & 0.99 & \textbf{1.00} & 0.98\\
        Lucerne  & 0.82 & 0.94 & 0.07 & 0.98 & 0.91 & 0.85 & \textbf{0.99} & 0.97\\
        Barley   & 0.73 & 0.77 & 0.76 & 0.89 & 0.97 & 0.93 & \textbf{0.98} & 0.95\\
        Potato   & 0.71 & 0.31 & 0.31 & 0.89 & 0.69 & 0.70 & \textbf{0.92} & 0.90\\
        Beet     & 0.32 & 0.27 & 0.32 & 0.56 & 0.68 & 0.63 & \textbf{0.90} & 0.75\\ \hline  \noalign{\vskip 1mm} 
        \end{tabularx}
\end{table}

\begin{table}[!t]
    \caption {Table of Accuracy, Kappa and Confidence.}
    \label{tab:OAnACC}
    \centering
    \begin{tabular}{l c c c}
        \hline \noalign{\vskip 0.5mm} 
        Band(s) & Accuracy ($\%$) & Kappa & Confidence \\ \hline \noalign{\vskip 1mm} 
        $C$ & 89.78 & 0.62 & 0.86\\
        $L$ & 90.86 & 0.64 & 0.80\\ 
        $P$ & 90.75 & 0.65 & 0.87\\ 
        $CL$ & 95.58 &  0.84 & 0.92\\ 
        $LP$ & 89.79 & 0.62 & 0.92\\ 
        $CP$ & 96.59 & 0.88 & 0.91\\
        ${CLP}$ (Proposed) & {98.23} & {0.94} & {0.93}\\ 
        $CLP_{+}$ & 95.23 & 0.89 & 0.92\\ \hline  \noalign{\vskip 1mm} 
        $SVM-RBF$ & 96.48 & 0.89 & --- \\ 
        $ELM$ & 95.70 & 0.82 & --- \\ \hline
    \end{tabular}
\end{table}




\begin{table}[!t]
    \centering
    \caption{Confusion Matrix for $CLP$}
    \label{tab:cmCLP}
    \begin{tabularx}{\columnwidth}{XXXXXXXX|X}
        \hline \noalign{\vskip 0.5mm} 
        $(\%)$   & Peas & Wheat      & R.Seed   & Lucerne      & Barley     & Potato   & Beet & Supp.     \\ \hline \noalign{\vskip 0.3mm} 
        Pees     & 100 & 0.00  & 0.00     & 0.00   & 0.00   & 0.00   & 0.00 & 2055 \\
        Wheat    & 0.00   & 93.90 & 0.04     & 0.03   & 5.96   & 0.00   & 0.07 & 56060 \\
        R.Seed   & 0.00   & 0.00  & 100   & 0.00   & 0.00   & 0.00   & 0.00 & 25994 \\
        Lucerne  & 0.00   & 0.00  & 0.00     & 99.88  & 0.00   & 0.00   & 0.12 & 4935 \\
        Barley   & 0.00   & 2.03  & 0.00     & 0.00   & 97.93  & 0.00   & 0.04 & 45497 \\
        Potato   & 0.00   & 0.00  & 0.78     & 0.00   & 7.15   & 91.97  & 0.10 & 17035 \\
        Beet     & 0.00   & 3.63  & 0.07     & 3.63   & 2.82   & 0.04   & 89.81 & 14974 \\ \hline  Prec.  & 2055   & 54107 &   26160     &   5489    &  49537      &15673  & 13529 &  \\ Overall  & Acc.   & 98.23\% &        &       &        &$\kappa$   & 0.94 &  \\ \hline
    \end{tabularx}
\end{table}






































\section{Conclusion}
In this paper, a novel tensorization framework in conjunction with an ANN algorithm is proposed to leverage the increased information content present in multi-frequency PolSAR data. 
The Kronecker product is used to combine information from multi-frequency PolSAR data. The resultant combination is efficiently utilized by an ANN. 
This is demonstrated on a C-, L-, P-band data-set for crop and forest classification.
Pairwise tensor combinations of frequency bands ($CL$, $CP$, $LP$) performs better than the individual band, which in turn, is outperformed by the tensorized triplet ($CLP$). This shows that the tensorized combination of frequency bands have increased information content, which leads to a better classification performance when used in conjunction with a ANN architecture. 
Moreover, the $CLP$ combination outperforms simple band augmentation ($CLP_{+}$).
In the future, this technique can be exploited for multi-temporal and multi-incidence datasets from advanced sensors for improved classification performance. 


































\ifCLASSOPTIONcaptionsoff
  \newpage
\fi






\bibliographystyle{IEEEtran}
\bibliography{001_References+IEEEabrv,002_References+agricultureSAR2,003_References+deepLearning2,004_References+ml_books,005_References+polsar2}














\end{document}


